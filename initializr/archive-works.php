<?php get_header(); ?>
<main>


<section class="pageHeader relative">
    <div class="pageHeaderImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_works_fv.jpg');"></div>
    <div class="pageHeaderText absolute bgTraColorDeep">
        <h2 class="h2 bold mb10">施工事例</h2>
        <p class="fontEn h1 mainColor">Works</p>
    </div>
</section>





<section class="pageWorkLi margin">
	<div class="container">
		<div class="pageWorksLiLiNavi mb50">
			<ul class="inlineBlockUl white archiveWorksCateUl text-center text_m bold">
			
			
				<li><a href="<?php echo home_url();?>/works">全て</a></li>
				<?php $categories = get_categories(array('taxonomy' => 'works_cat')); if ( $categories ) : ?>
					<?php foreach ( $categories as $category ): ?>
						<li><a href="<?php echo home_url();?>/works_cat/<?php echo esc_html( $category->slug);?>"><?php echo wp_specialchars( $category->name ); ?></a></li>
					<?php endforeach; ?>
				<?php endif; ?>
								
			</ul>
		</div>
        <div class="row">
			<?php			
				while ( have_posts() ) : the_post();
					get_template_part('content-post-works-archive'); 
				endwhile;
			?>
        </div>
		
		
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>









</main>






<?php get_footer(); ?>
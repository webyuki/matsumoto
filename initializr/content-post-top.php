<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>


					<li>
						<a href="<?php the_permalink();?>">
							<div class="topNewsUlBox tra">
								<p class="text_m fontNum"><?php the_time('y/m/d'); ?><span class="topNewsCate"><?php echo $cat_name; ?></span></p>
								<p class="mainColor bold"><?php the_title();?></p>
							</div>
						</a>
					</li>
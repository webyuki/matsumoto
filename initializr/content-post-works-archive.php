
            <div class="col-sm-4">
                <a href="<?php the_permalink(); ?>" class="">
                    <div class="topWorksBox mb50">
                    
                        <?php if (has_post_thumbnail()):?>
                        <?php 
                            // アイキャッチ画像のIDを取得
                            $thumbnail_id = get_post_thumbnail_id();
                            // mediumサイズの画像内容を取得（引数にmediumをセット）
                            $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
                            $eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
                        ?>
                            <div class="topWorksImg bgImg mb10 relative tra" style="background-image:url('<?php echo $eye_img_s[0];?>')">
                        <?php else: ?>
                            <div class="topWorksImg bgImg mb10 relative tra" style="background-image:url('<?php echo get_template_directory_uri();?>/img/sample01.png')">
                        <?php endif; ?>
                    
                                <div class="topWorksCate bgWhite absolute text_m bold">
                                    <h5 class="bold h4 mainColor"><?php the_title(); ?></h5>
                                </div>
                            </div>
                        <ul class="topWorksUl text_m inlineBlockUl gray">
                        
                            <?php 
                                if ($terms = get_the_terms($post->ID, 'works_area')) {
                                    foreach ( $terms as $term ) {
                                        echo '<li>' . esc_html($term->name) .'</li>';
                                    }
                                }
                            ?>
                            <?php 
                                if ($terms = get_the_terms($post->ID, 'works_cat')) {
                                    foreach ( $terms as $term ) {
                                        echo '<li>' . esc_html($term->name) .'</li>';
                                    }
                                }
                            ?>
                        
                        </ul>
                    </div>
                </a>
            </div>
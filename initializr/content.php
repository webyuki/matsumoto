<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>


<section class="pageHeader relative">
    <div class="pageHeaderImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_works_fv.jpg');"></div>
    <div class="pageHeaderText absolute bgTraColorDeep">
        <h2 class="h2 bold mb10">新着情報</h2>
        <p class="fontEn h1 mainColor">News</p>
    </div>
</section>


<section class="single margin">
	<div class="container">
		<?php //get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
			<article class="singleArticle mb50">
				<h2 class="bold h2"><?php the_title();?></h2>
				<p class="cateDate pageNewsCate text-right">
					<span class="cate white text_s <?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span><span class="date subColor questrial italic text_s"><?php the_time('y/m/d'); ?></span>
				</p>
				
					<?php if (has_post_thumbnail()): ?>
					<?php 
						// アイキャッチ画像のIDを取得
						$thumbnail_id = get_post_thumbnail_id(); 
						// mediumサイズの画像内容を取得（引数にmediumをセット）
						$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
						$eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
					?>
						<img class="" src="<?php echo $eye_img_s[0];?>">
						<?php else: ?>
						<img class="" src="<?php echo get_template_directory_uri();?>/img/sample01.png">
						<?php endif; ?>	
				
				<div class="entry"><?php the_content();?></div>				
				<?php get_template_part( 'parts/temp-sns-share' ); ?>				
				
				<p class="bold h5 mb10"><i class="fa fa-folder-open"></i>カテゴリー</p>
				<div class="text_m gray mb30">
					<?php the_category(', '); ?>
				</div>
				<p class="bold h5 mb10"><i class="fa fa-tags"></i>タグ</p>
				<?php the_tags('<ul class="text_m gray"><li>','</li><li>','</li></ul>'); ?>
				
				
			</article>
			
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

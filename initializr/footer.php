
<section class="footerMenu">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <a href="<?php echo home_url();?>/about-us">
                    <div class="bg-scale footerMenuBox box1">
                        <div class="inner bgTraColorDeep relative">
                            <div class="text-center footerMenuText">
                                <p class="h4 bold mb10 matchHeight">松本工務店について</p>
                                <p class="fontEn h4 mb0 mainColor">About Us</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3">
                <a href="<?php echo home_url();?>/service">
                    <div class="bg-scale footerMenuBox box2">
                        <div class="inner bgTraColorDeep relative">
                            <div class="text-center footerMenuText">
                                <p class="h4 bold mb10 matchHeight">サービス内容</p>
                                <p class="fontEn h4 mb0 mainColor">Service</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3">
                <a href="<?php echo home_url();?>/works">
                    <div class="bg-scale footerMenuBox box3">
                        <div class="inner bgTraColorDeep relative">
                            <div class="text-center footerMenuText">
                                <p class="h4 bold mb10 matchHeight">施工事例</p>
                                <p class="fontEn h4 mb0 mainColor">Works</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-3">
                <a href="<?php echo home_url();?>/recruit">
                    <div class="bg-scale footerMenuBox box4">
                        <div class="inner bgTraColorDeep relative">
                            <div class="text-center footerMenuText">
                                <p class="h4 bold mb10 matchHeight">採用情報</p>
                                <p class="fontEn h4 mb0 mainColor">Recruit</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_concept_bg.jpg">
        <div class="bgBlueTrans padding">
            <div class="container" data-aos="fade-up">
                <div class="text-center white">
                    <p class="fontEn h2 mb10">Contact</p>
                    <p class="h3 bold mb30">リフォーム・新築・家のことなら<br>お気軽にお問い合わせください</p>
                </div>
                <ul class="footerContactUl flex justCenter">
                    <li>
                        <div class="footerContactUlBox bgSubColor matchHeight">
                            <div class="text-center">
                                <p class="h4 bold mb20 mainColor titleBdThin">メールフォームから<br>お問い合わせ</p>
                            </div>
                            <p class="text_m gray mb30">お見積もり依頼や詳しいご相談をされたい場合にはお問い合わせフォームをご活用ください。</p>
                            <div class="text-center">
                                <a href="<?php echo home_url();?>/contact" class="white button h4 mb10 bold tra text-center">お問い合わせ</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="footerContactUlBox bgSubColor matchHeight">
                            <div class="text-center">
                                <p class="h4 bold mb20 mainColor titleBdThin">今すぐ電話で<br>問い合わせる</p>
                            </div>
                            <p class="text_m gray mb20">平日9時〜17時であれば、お電話対応可能です。どんなご質問でもお答えいたします。</p>
                            <div class="text-center">
                                <a href="tel:0869250084">
                                    <p class="fontEn mainColor h1"><i class="fa fa-phone" aria-hidden="true"></i>0869-25-0084</p>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>




<section class="sectionPankuzu">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
	</div>
</section>

<footer class="bgSubColor">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="text_m">
                    <p class="">瀬戸内でリフォーム・新築施工の工務店</p>
                    <a href="<?php echo home_url();?>"><p class="logo logo_footer remove mb10 ml0"><?php the_title();?></p></a>

                    <p class="mb30">〒701-4501<br>岡山県瀬戸内市邑久町虫明531<a target="_blank" href="https://goo.gl/maps/JrhDF9EFnrSS7G3o9"><i class="fa fa-map-marker"></i></a><br>
                        TEL：0869-25-0084<br>FAX：0869-25-0196<br>
                        </p>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="mb30 footerTextMenuBox">
                            <p class="mainColor bold fontEn footerTextMenuTitle">About Us</p>
                            <ul class="text_m footerTextMenuUl">
                                <li>
                                    <a href="<?php echo home_url();?>/about-us"><p class="textHoverLeft">私たちについて</p></a>
                                </li>
                                <li>
                                    <a href="<?php echo home_url();?>/recruit"><p class="textHoverLeft">採用情報</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="mb30 footerTextMenuBox">
                            <p class="mainColor bold fontEn footerTextMenuTitle">Service</p>
                            <ul class="text_m footerTextMenuUl">
                                <li>
                                    <a href="<?php echo home_url();?>/service"><p class="textHoverLeft">サービス内容</p></a>
                                </li>
                                <li>
                                    <a href="<?php echo home_url();?>/works"><p class="textHoverLeft">施工事例</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="mb30 footerTextMenuBox">
                            <p class="mainColor bold fontEn footerTextMenuTitle">Information</p>
                            <ul class="text_m footerTextMenuUl">
                                <li>
                                    <a href="<?php echo home_url();?>/news"><p class="textHoverLeft">新着情報</p></a>
                                </li>
                                <li>
                                    <a href="<?php echo home_url();?>/contact"><p class="textHoverLeft">お問い合わせ</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
        <div class="wrapper_copy">
            <div class="container">
                <p class="text-center text_s gray">copyright© 2019 matsumoto koumuten all rights reserved.</p>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



<script>
AOS.init({
	placement	: "bottom-top",
	duration: 1000,
});
</script>
</body>
</html>
 
<?php get_header(); ?>
<main>


<section class="topFvSection relative">
    <div class="topFv relative" data-aos="fade-left">
        <div class="main_imgBox">
            <div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_01.jpg');"></div>
            <div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_02.jpg');"></div>
            <div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_03.jpg');"></div>
        </div>
    </div>
    <div class="topFvBoxText absolute bgTraColorDeep" data-aos="fade-right">
        <h2 class="h2 bold mb10">住む人・使う人の目線で、<br>誠実な家づくりを60年</h2>
        <p class="fontEn h1 mainColor">High Quality House Building</p>
    </div>
</section>



<section class="relative margin" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_concept_bg.jpg">
        <div class="container" data-aos="fade-up">
            <div class="padding">
                <div class="bgTraColor width720">
                    <div class="boxInnerBd">
                        <div class="text-center">
                            <p class="fontEn h2 mb10 mainColor">Concept</p>
                            <h3 class="h3 bold mb30">瀬戸内の街を支える<br>技術と実績を誇りに</h3>
                        </div>
                        <div class="mb50">
<p>創業から60年、松本工務店は発展する瀬戸内市周辺の公共施設や店舗、オフィスの建設を数多く手がけ、地域との絆を深めてまいりました。</p>
<p>一般住宅の新築やリフォーム工事にも、新しい時代に求められる厳しいルールを守り、誠実に取り組んでいます。</p>
<p>私たちは、公共工事と一般住宅の施工両面から地域の暮らしを支える仕事を誇りに、これからもこの地で営業を続けてまいります。</p>

                        </div>
                        <div class="text-center">
                            <a href="<?php echo home_url();?>/about-us" class="white button h4 mb10 bold tra text-center">松本工務店の想い</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="">
    <div class="container">
        <div class="mb50">
            <p class="fontEn h2 mb0 mainColor">Service</p>
            <h3 class="h3 bold titleBd inlineBlock">サービス内容</h3>
        </div>
    </div>
</section>



<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_service_bg_01.jpg">
        <div class="bgTraColorDeep topServiceTextBox">
            <div class="topServiceTextInner">
                <div class="topServiceTextBd">
                    <div class="">
                        <h3 class="h5 mb10">リフォーム</h3>
                        <p class="fontEn h2 mb30 mainColor">Reform</p>
                        <p class="h3 bold mb30">予算・住み心地・使いやすさ<br>を叶えるリフォーム</p>
                    </div>
                    <div class="">
                        <a href="<?php echo home_url();?>/service/#reform" class="white button h4 mb10 bold tra text-center">リフォーム</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="relative margin" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_service_bg_02.jpg">
        <div class="bgTraColorDeep topServiceTextBox">
            <div class="topServiceTextInner">
                <div class="topServiceTextBd">
                    <div class="">
                        <h3 class="h5 mb10">新築施工</h3>
                        <p class="fontEn h2 mb30 mainColor">Build</p>
                        <p class="h3 bold mb30">冬あたたかく、夏さわやかに<br>快適性を重視した家づくり</p>
                    </div>
                    <div class="">
                        <a href="<?php echo home_url();?>/service/#build" class="white button h4 mb10 bold tra text-center">新築施工</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="relative margin" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_service_bg_03.jpg">
        <div class="bgTraColorDeep topServiceTextBox">
            <div class="topServiceTextInner">
                <div class="topServiceTextBd">
                    <div class="">
                        <h3 class="h5 mb10">店舗・事務所・公共施設</h3>
                        <p class="fontEn h2 mb30 mainColor">For Business</p>
                        <p class="h3 bold mb30">店舗・事務所・公共施設など、<br>地域に密着した豊富な施工実績</p>
                    </div>
                    <div class="">
                        <a href="<?php echo home_url();?>/service/#shop" class="white button h4 mb10 bold tra text-center">店舗・事務所・公共施設</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="mb50 text-center">
            <p class="fontEn h2 mb0 mainColor">Works</p>
            <h3 class="h3 bold titleBd inlineBlock">施工事例</h3>
        </div>
        <div class="row mb30" data-aos="fade-up">
        	<?php
						$args = array(
                            'post_type' => 'works', //投稿タイプ名
                            'posts_per_page' => 6 //出力する記事の数
						);
					?>
                    <?php
                    $myposts = get_posts( $args );
                	foreach ( $myposts as $post ) : setup_postdata( $post );
					?>

			<?php get_template_part('content-post-works-archive'); ?>

            <?php endforeach; ?>
        </div>
        <div class="text-center">
            <a href="<?php echo home_url();?>/works" class="white button h4 mb10 bold tra text-center">施工事例</a>
        </div>
    </div>
</section>

<section class="topRecruit margin relative">
    <div class="container">
        <div class="row">
            <div class="col-sm-6" data-aos="fade-right">
                <div class="mb30">
                    <p class="fontEn h2 mb0 mainColor">Recruit</p>
                    <h3 class="h3 bold titleBd inlineBlock">採用情報</h3>
                </div>
                <p class="h3 mainColor bold mb30">お客さまの安心と、現場の安全を守る</p>
                <div class="mb50">
                    <p>松本工務店では安全第一で工事を行い、お客さまが安心して過ごせる建物を建築しています。想いを共にする方からの応募を、心からお待ちしています！</p>
                </div>
                <div class="mb50">
                    <a href="<?php echo home_url();?>/recruit" class="white button h4 mb10 bold tra text-center">採用情報</a>
                </div>
            </div>
            <div class="col-sm-6" data-aos="fade-left">
                <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/top_recruit_01.jpg" alt="採用情報">
                <div class="row">
                    <div class="col-xs-6">
                        <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/top_recruit_02.jpg" alt="採用情報">
                    </div>
                    <div class="col-xs-6">
                        <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/top_recruit_03.jpg" alt="採用情報">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="topQa margin">
	<div class="container">
        <div class="mb50">
            <p class="fontEn h2 mb0 mainColor">Q&amp;A</p>
            <h3 class="h3 bold titleBd inlineBlock">よくあるご質問</h3>
        </div>
        <div class="topQaBox">
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li class="mainColorLight bold">見積は無料ですか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li><p>新築・リフォーム問わず、お見積は無料です。お気軽にお問い合わせください。</p></li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li class="mainColorLight bold">施工期間はどれくらいですか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li><p>新築の着工から竣工まではおよそ4ヶ月前後とご案内していますが、建物の大きさや面積、プラン、材料の納期によって工事にかかる期間は変わります。またリフォームの場合も内容によってかかる日にちは変わります。お客さまのご都合やご希望をお気軽にご相談ください。</p></li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li class="mainColorLight bold">リノベーションはできますか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li><p>弊社ではリフォームだけでなく、増築やリノベーションなどにも対応しています。古民家のリノベーションなどの実績もあります。お気軽にご相談ください。</p></li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li class="mainColorLight bold">どの地域で施工をしていますか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li><p>現在は瀬戸内市を中心に、岡山市や備前市などで施工をしています。地域はご相談させていただけますので、まずはお問い合わせください。</p></li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li class="mainColorLight bold">予算があまりありません。依頼はできますか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li><p>弊社では、お客さまのご予算に合わせたプランを作成しています。まずはご希望のご予算をお知らせください。</p></li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li class="mainColorLight bold">アフターメンテナンスはありますか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li><p>住宅の保証期間中はもちろん、お客さまとは施工後も長くお付き合いさせていただいています。</p></li>
					</ul>
				
				</dd>
			</dl>

		</div>
	</div>
</section>


<section class="topNews padding bgSubColor" id="news">
	<div class="container">
		<div class="row">
			<div class="col-sm-4" data-aos="fade-right">
                <div class="mb50">
                    <p class="fontEn h2 mb0 mainColor">News</p>
                    <h3 class="h3 bold">新着情報</h3>
                </div>
                <div class="mb30 pc">
                    <a href="<?php echo home_url();?>/news" class="white button h4 mb10 bold tra text-center">新着情報</a>
                </div>
			</div>
			<div class="col-sm-8" data-aos="fade-left">
				<ul class="topNewsUl mb30">
				
		
					<?php
						//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
						$paged = get_query_var('page');
						$args = array(
							'post_type' =>  'post', // 投稿タイプを指定
							'paged' => $paged,
							'posts_per_page' => 3, // 表示するページ数
							'orderby'=>'date',
							/*'cat' => -5,*/
							'order'=>'DESC'
									);
						$wp_query = new WP_Query( $args ); // クエリの指定 	
						while ( $wp_query->have_posts() ) : $wp_query->the_post();
							//ここに表示するタイトルやコンテンツなどを指定 
						get_template_part('content-post-top'); 
						endwhile;
						//wp_reset_postdata(); //忘れずにリセットする必要がある
						wp_reset_query();
					?>		
		
				
				
				</ul>
			</div>
		</div>
        <div class="sp">
            <a href="<?php echo home_url();?>/news" class="white button h4 mb10 bold tra text-center">新着情報</a>
        </div>
	</div>
</section>





</main>



<?php get_footer(); ?>
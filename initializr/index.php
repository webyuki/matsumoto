<?php get_header(); ?>
<main>

<section class="pageHeader relative">
    <div class="pageHeaderImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_works_fv.jpg');"></div>
    <div class="pageHeaderText absolute bgTraColorDeep">
        <h2 class="h2 bold mb10">新着情報</h2>
        <p class="fontEn h1 mainColor">News</p>
    </div>
</section>






        

<section class="pageNews margin">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>
</main>
<?php get_footer(); ?>
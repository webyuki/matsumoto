<?php get_header(); ?>

<main>

<section class="pageHeader relative">
    <div class="pageHeaderImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_about_fv.jpg');"></div>
    <div class="pageHeaderText absolute bgTraColorDeep" data-aos="fade-up">
        <h2 class="h2 bold mb10">松本工務店について</h2>
        <p class="fontEn h1 mainColor">About Us</p>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="row">
            <div class="col-sm-6" data-aos="fade-right">
                <div class="pageAbutTextBd">
                    <p class="fontEn h2 mainColor">Message</p>
                    <h3 class="h3 bold mb30">代表あいさつ</h3>
                    <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">堅実な工事と、<br>誠実な家づくりをこれからも</h4>
                    <div class="mb30">
                    
<p>松本工務店は創業から60年、長く地域の公共工事に携わってきました。</p>
<p>発展する瀬戸内の街とともに、公共施設や住宅等を数多く手がけ、地域にお住いの皆さまとの絆を深めてまいりました。</p>
<!--
<p class="mb30">「夏は公共工事、冬は一般住宅」と、お客さまに住宅の施工をお待ちいただきながら、駆け回っていた時代もあります。</p>

<p>お客さまにご負担をおかけすることを心苦しく思いながらも、公共工事における審査の厳しさに長く向き合うことで、私たちは建築に携わる姿勢を常に問われ続けてきました。</p>
-->
                        
<p>一時期は上半期は住宅に、下半期は公共工事に数多く手掛けて広範囲に駆け回っていた頃が懐かしく思います。</p>
<p class="mb30">おかげで一般住宅の施工に関しても、新しい時代に誕生した厳格なルールを負担に感じることなく、誠実な家づくりを続けることができています。</p>

<p>公共工事と一般住宅、両輪で駆け抜けた時代の後に、私たちは地域の暮らしをこれからもしっかりと支え、守っていく覚悟を新たにしています。</p>


                    </div>
                </div>
            </div>
            <div class="col-sm-6" data-aos="fade-left">
                <div class="pageAboutMessageImgBox">
                    <img class="pageAboutMessageImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_about_message_01.jpg" alt="代表取締役 松本 一">
                    <div class="text-right">
                        <p class="text_m mb0">代表取締役</p>
                        <p class="h3 bold">松本 一</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="">
    <div class="container">
        <div class="mb50">
            <p class="fontEn h2 mb0 mainColor">Cencept</p>
            <h3 class="h3 bold titleBd inlineBlock">仕事へのこだわり</h3>
        </div>
    </div>
</section>


<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_about_conc_01.jpg">
        <div class="bgTraColorDeep topServiceTextBox">
            <div class="topServiceTextInner">
                <div class="topServiceTextBd">
                    <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">予算と希望を叶える<br>柔軟なプランを作成</h4>
                    <div>
                    
<p>松本工務店では、お客さまのご予算にあわせ、無理なく実現できるプランをご提案しています。</p>
<p>新築でもリフォームでも、施工にかかる費用は大きな負担となりがちです。</p>
<p>不安やご希望があれば、どんなことでもご相談ください。</p>
<p>経済面からも快適な暮らしを守るため、長年の経験で培った知識をいかし、柔軟なプランを作成いたします。</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="relative margin" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_about_conc_02.jpg">
        <div class="bgTraColorDeep topServiceTextBox even">
            <div class="topServiceTextInner">
                <div class="topServiceTextBd">
                    <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">高品質な技術と設備で<br>住み心地と使い勝手に配慮</h4>
                    <div>
                    
<p>「予算は抑えつつ、できるだけよいものを取り入れたい」。これはどのお客さまにも共通する想いです。</p>
<p>一方で私たちは数多くの工事を手がけた経験から、高価なものが必ずしもよいものではないことを知っています。</p>
<p>松本工務店では、豊富な知識からお客さまに最適な設備をお選びし、より快適な生活をサポートしてまいります。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="relative margin" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_about_conc_03.jpg">
        <div class="bgTraColorDeep topServiceTextBox">
            <div class="topServiceTextInner">
                <div class="topServiceTextBd">
                    <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">コミュニケーション能力と、<br>技術力の高い職人を採用</h4>
                    <div>
                    
<p>家づくりは、お客さまの想いを叶える仕事です。</p>
<p>関わる職人には高い技術力だけでなく、お客さまや施工に関わる人との密なコミュニケーションが求められます。</p>
<p>松本工務店ではお客さまと信頼関係を築き、的確に施工に反映させていける職人と連携し、高品質なサービスを提供しています。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="mb50 text-center">
            <p class="fontEn h2 mb0 mainColor">Company</p>
            <h3 class="h3 bold titleBd inlineBlock">会社概要</h3>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="pageAboutCompanyUl mb50">
                    <ul>
                        <li>会社名</li>
                        <li>株式会社松本工務店</li>
                    </ul>
                    <ul>
                        <li>代表者名</li>
                        <li>松本 一</li>
                    </ul>
                    <ul>
                        <li>住所</li>
                        <li>〒701-4501<br>岡山県瀬戸内市邑久町虫明531</li>
                    </ul>
                    <ul>
                        <li>連絡先</li>
                        <li>TEL：0869-25-0084<br>FAX：0869-25-0196</li>
                    </ul>
                    <ul>
                        <li>営業時間</li>
                        <li>9：00～17：00</li>
                    </ul>
                    <ul>
                        <li>事業内容</li>
                        <li>建設設計業務全般</li>
                    </ul>
                    <ul>
                        <li>有資格者</li>
                        <li>		
一級建築士 2名<br>
二級建築士 1名<br>
一級建築施工管理技士 4名<br>
一級土木施工管理技士 5名<br>
一級管工事施工管理技士	1名<br>
二級管工事施工管理技士	1名<br>
一級造園施工管理技士 1名

                        </li>
                    </ul>
                    <ul>
                        <li>建設業許可</li>
                        <li>岡山県知事許可（特-27）第6915号</li>
                    </ul>
                    <ul>
                        <li>特定建設業の種類</li>
                        <li>土木工事業・建築工事業・大工工事業・左官工事業・とび・石工事業・屋根工事業・タイル、れんが、ブロック工事業・鋼構造物工事業・鉄筋工事業・舗装工事業・しゅんせつ工事業・板金工事業・ガラス工事業・塗装工事業・防水工事業・内装仕上工事業・熱絶縁工事業・建具工事業・水道施設工事業・解体工事業	</li>
                    </ul>
                    <ul>
                        <li>主要取引先</li>
                        <li>岡山県・瀬戸内市・厚生労働省　他</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_about_company_01.jpg" alt="株式会社松本工務店">
            </div>
        </div>
        <div class="map">
            <!--
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11705.193838654022!2d134.1993032861392!3d34.68628307292194!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x35546ee324b6cf5d%3A0x2c45bea244c20a32!2z44CSNzAxLTQ1MDEg5bKh5bGx55yM54Cs5oi45YaF5biC6YKR5LmF55S66Jmr5piO77yV77yT77yR!5e0!3m2!1sja!2sjp!4v1580443757179!5m2!1sja!2sjp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8154.361890755292!2d134.2012960720504!3d34.685304655514976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x35546ee329be8fd5%3A0x9342d112718c77aa!2z44CSNzAxLTQ1MDEg5bKh5bGx55yM54Cs5oi45YaF5biC6YKR5LmF55S66Jmr5piO77yU77yU77yT77yT!5e0!3m2!1sja!2sjp!4v1596419936629!5m2!1sja!2sjp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>
<?php get_header(); ?>

<main>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_build_fv.jpg">
        <div class="bgWhiteTrans paddingW">
            <div class="container" data-aos="fade-up">
                <div class="text-center">
                    <p class="fontEn h3 mb0 mainColor">BUILD</p>
                    <h3 class="h2 bold">新築施工</h3>
                </div>
            </div>
        </div>
    </div>
</section>






<section class="padding bgSubColor">
    <div class="container">
        <div class="text-center mb50">
            <h3 class="h3 bold">家を建てたいけど、こんなお悩みはありませんか？</h3>
        </div>
        <div class="width720 bgWhite" data-aos="fade-up">
            <ul class="pageReformTroubleUl">
                <li><i class="fa fa-check-circle h2 mainColorLight" aria-hidden="true"></i><span class="bold h4">理想を叶えるフルオーダーの家を建てたい</span></li>
                <li><i class="fa fa-check-circle h2 mainColorLight" aria-hidden="true"></i><span class="bold h4">費用を抑えつつ、機能性の高い設備を選びたい</span></li>
                <li><i class="fa fa-check-circle h2 mainColorLight" aria-hidden="true"></i><span class="bold h4">アフターメンテナンスに強い会社がいい</span></li>
            </ul>
        </div>
    </div>
</section>


<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">ADVANTAGE</p>
            <h3 class="h3 bold">オカザキリフォームラボの新築施工の魅力</h3>
        </div>
        <div class="row pageAboutFeatureRow" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_build_01.jpg" alt="">
                    <p class="pageAboutFeatureNum absolute fontEn h00 mainColor">01</p>
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">リフォームのスペシャリストが<br>提案する、失敗のない家</span></h4>
                <div class="mb30">

<p>オカザキリフォームラボは、リフォームのスペシャリストとして数多くの実績を残してきました。</p>
<p>使用する設備についても、つねに新しい情報にふれ、商品についての研究を重ねています。</p>
<p>お客さまのお悩みを聞き、ひとつ一つ解決してきた経験をいかし、はじめての新築でも安心のプランをご提案させていただきます。</p>
                </div>
            </div>
        </div>
        <div class="row pageAboutFeatureRow" data-aos="fade-up">
            <div class="col-sm-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_reform_01.jpg" alt="">
                    <p class="pageAboutFeatureNum absolute fontEn h00 mainColor left">02</p>
                </div>
            </div>
            <div class="col-sm-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">創業から90年をかけて培った、<br>幅広いネットワーク</span></h4>
                <div class="mb30">

<p>オカザキリフォームラボは、建築資材を取り扱ってきた90年の実績から、設計士やデザイナー、職人といった数多くのプロフェッショナルとのネットワークを構築しています。</p>
<p>様々なジャンルに秀でた専門家と連携し、お客さまの細やかなご要望にお応えすることが可能です。</p>
                </div>
            </div>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="pageAboutFeatureImgBox relative">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_build_03.jpg" alt="">
                    <p class="pageAboutFeatureNum absolute fontEn h00 mainColor">03</p>
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">収納や水まわりなど、<br>空間の効率的な活用方法をご提案</span></h4>
                <div class="mb30">

<p>一軒の住宅には、水にまつわる技術や知識を必要とするキッチン・浴室・トイレや、空間活用の知識を求められる収納など、さまざまな用途を持つスペースが共存しています。</p>
<p>オカザキリフォームラボではそのひとつ一つに注力してきた実績をいかし、すべてのスペースを有効に活用するご提案をしています。</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding bgMainColorLight">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">POINT</p>
            <h3 class="h3 bold">どんなご要望でもお任せください</h3>
        </div>
        <div class="row mb30" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_build_sample_02.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">モダン</h4>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_build_sample_03.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">シンプル</h4>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_build_sample_01.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">ナチュラル</h4>
                </div>
            </div>
        </div>
        <div class="row mb30" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_build_sample_04.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">北欧スタイル</h4>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_build_sample_05.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">ビンテージ</h4>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_build_sample_06.jpg" alt="">
                    <h4 class="h4 bold mainColorLight text-center mb10">カリフォルニアスタイル</h4>
                </div>
            </div>
        </div>
        <div class="text-center">
            <a href="<?php echo home_url();?>/works" class="white button bold tra text-center">新築施工の実績を見る</a>
        </div>
    </div>
</section>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_reform_voice_bg.jpg">
        <div class="bgWhiteTrans padding">
            <div class="container" data-aos="fade-up">
                <div class="text-center mb50">
                    <p class="fontEn h4 mb0 mainColor">VOICE</p>
                    <h3 class="h3 bold">お客様の声</h3>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50 matchHeight">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_build_voice_01.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">以前の家からずっと見てもらっています。</h5>
                                <p class="grayLight bold">岡山市 S様</p>
                            </div>
                            <p class="text_m gray">昔から住んでいた家が老朽化していたので床下を点検してもらったところ、かなり危険な状態であることがわかりました。はじめは修理のつもりでしたが、あれこれ相談しているうちにオカザキリフォームラボさんなら新築を任せられると思い、建て替えの工事をお願いしました。アフターメンテナンスも安心です。</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50 matchHeight">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_build_voice_02.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">とにかくいい！しかありません。</h5>
                                <p class="grayLight bold">岡山市 F様</p>
                            </div>
                            <p class="text_m gray">夏は暑いのが当たり前だと思っていましたが、新しい家は2階の子ども部屋でエアコンをつけると1階まで冷えて驚いています。高気密高断熱をすすめられ、なんとなく「いいのかな？」と思っていましたが、住んでみて良さを実感しています。</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50 matchHeight">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_build_voice_03.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">収納がぴったり。</h5>
                                <p class="grayLight bold">岡山市 T様</p>
                            </div>
                            <p class="text_m gray">知り合いからは、はじめて建てる家は、後からいろいろ反省することが多いと聞いていましたが、オカザキリフォームラボさんはどこにどんな収納を用意したほうがいいなど、細かいアドバイスをしてくれました。おかげで必要なものがぴったり納まり、家のなかはいつもスッキリしています。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding bgSubColor">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">FLOW</p>
            <h3 class="h3 bold">新築施工の流れ</h3>
        </div>
        <div class="width980 bgWhite pageReformFlowWrap" data-aos="fade-up">
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_01.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">お問い合わせ</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 01</p>
                    <p>お電話やWebサイトのお問い合わせフォームからご連絡ください。パソコン・スマートフォンのいずれでもお問い合わせいただけます。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_build_flow_02.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">土地探し</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 02</p>
                    <p>土地が決まっていないお客さまには不動産会社をご紹介し、土地探しからお手伝いします。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_build_flow_03.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">地盤調査</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 03</p>
                    <p>調査会社による地盤調査を実施します。地盤改良が必要な場合は見積をご提示いたします。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_02.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">ヒアリング・打ち合わせ</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 04</p>
                    <p>お客さまのご要望をうかがい、ご要望にあう設計士やデザイナーと連携し、立地をいかしたプランを作成します。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_04.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">ご契約</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 05</p>
                    <p>プランをご了承いただき、契約書に同意をいただいたうえで契約を締結します。ご契約後に確認申請と工事の準備を進めます。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_05.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">着工（工事開始）</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 06</p>
                    <p>プランに従って施工を行います。工事に関するご相談があればいつでもご連絡ください。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_06.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">お引き渡し</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 07</p>
                    <p>お客さま立ち合いのもと最終確認を実施し、お引き渡しを行います。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_reform_flow_07.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">アフターメンテナンス</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 08</p>
                    <p>施工後は、定期的にメンテナンスを実施します。その他、気になる箇所があればいつでもご連絡ください。内容に応じてメンテナンスのご案内をいたします。</p>
                </div>
            </div>
        
        </div>
    </div>
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>
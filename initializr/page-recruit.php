<?php get_header(); ?>

<main>

<section class="pageHeader relative">
    <div class="pageHeaderImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_recruit_fv.jpg');"></div>
    <div class="pageHeaderText absolute bgTraColorDeep" data-aos="fade-up">
        <h2 class="h2 bold mb10">採用情報</h2>
        <p class="fontEn h1 mainColor">Recruit</p>
    </div>
</section>

<section class="padding bgSubColor margin">
    <div class="container">
        <div class="pageRecruitMessageBox bgWhite relative" data-aos="fade-up">
            <div class="text-center">
                <p class="fontEn h2 mainColor">Message</p>
                <h3 class="h3 bold mb30">代表からのメッセージ</h3>
                <h4 class="mainColor bold h3 mb50 titleBdThin inlineBlock">信頼する仲間と、<br>確かな品質を守っていきたい</h4>
            </div>
            <div class="width720">
                <div class="mb50">

<p>松本工務店は創業から60年、この瀬戸内の地で地域の暮らしを支えてまいりました。</p>
<p>一般住宅の建築やリフォームから公共工事まで。様々な工事に関わるなかで最も大切にしてきたのは、信頼できる職人さんと仕事をすること。</p>
<p>お客さまの安全を確実に守るため、技術力はもとより、関わる人と適切なコミュニケーションを取れる職人さんだけを選び、高品質な工事を行ってきました。</p>
<p>大規模な公共工事に数多く携わり、地域からも大きな信頼を得られたのは、ひとえに職人さんの誠実な仕事ぶりのおかげです。</p>
<p>これからも私たちは、品質に妥協することなく、お客さまからの信頼を守る職人さんを迎え、末永く地域への貢献を続けてまいります。</p>

                </div>
                <div class="pageRecruitMessagepagImg">
                    <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_recruit_message_01.jpg" alt="松本 一">
                </div>
                <div class="text-center">
                    <p class="text_m mb0">代表取締役</p>
                    <p class="h3 bold">松本 一</p>
                </div>
            </div>
            
        </div>
        
    </div>
</section>


<section class="margin">
    <div class="container">
        <div class="mb30">
            <p class="fontEn h2 mb0 mainColor">Wanted</p>
            <h3 class="h3 bold titleBd inlineBlock">求める人物像</h3>
        </div>
        <div class="row mb30" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">01</p>
                    <h5 class="mainColor text-center h4 bold mb30 matchHeight">円滑なコミュニケーション<br>が取れる人</h5>
                    <p class="text_m gray">お客さまの希望をしっかりとヒアリングし、施工に関わる人たちに的確に伝えていける人を求めています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">02</p>
                    <h5 class="mainColor text-center h4 bold mb30 matchHeight">向上心のある人</h5>
                    <p class="text_m gray">高品質なサービス提供のため、技術や知識を積極的に身につけ、忍耐強く努力できる人を求めています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">03</p>
                    <h5 class="mainColor text-center h4 bold mb30 matchHeight">資格や経験のある人</h5>
                    <p class="text_m gray">未経験者でも応募可能ですが、土木や建築関係の資格保有者や、経験者を歓迎しています。入社後の資格取得もバックアップします。</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="">
    <div class="container">
        <div class="mb50">
            <p class="fontEn h2 mb0 mainColor">Interview</p>
            <h3 class="h3 bold titleBd inlineBlock">わたしたちが働いています</h3>
        </div>
    </div>
</section>


<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_recruit_staff_01.jpg">
        <div class="bgTraColorDeep topServiceTextBox">
            <div class="topServiceTextInner">
                <div class="topServiceTextBd">
                    <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">長く、新しく続けていきたい</h4>
                    <p class="mainColor bold text_m mb0">一級建築士</p>
                    <p class="h3 bold mb30">松本 浩揮</p>
                    <div class="text_m">
                    
<p>建築会社に就職し、15年間現場監督を務める。2019年に退社後、父である一さんの後を継ぐため松本工務店に入社。前職での経験をいかして現場監督の仕事を続けている。今後は保有資格をいかした仕事や、新しい技術を取り入れた施工も視野に入れている。</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="topRecruit margin relative">
    <div class="container">
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6">
                <p class="gray bold">Q.どんなところにやりがいを感じますか？</p>
                <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">公共施設や店舗など<br>様々な種類の建物に関われる</h4>
                <div class="mb30">

<p>弊社では、土木建築の公共工事の新築・改修など、様々な種類の工事に関われるところにやりがいを感じます。</p>                    
                </div>
            </div>
            <div class="col-sm-6">
                <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_recruit_staff_02.jpg" alt="公共施設や店舗など様々な種類の建物に関われる">
            </div>
        </div>
    </div>
</section>


<section class="">
    <div class="container">
        <div class="mb50">
            <p class="fontEn h2 mb0 mainColor">Step</p>
            <h3 class="h3 bold titleBd inlineBlock">選考ステップ</h3>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="topWorksBox mb50">
                    <div class="bgSubColor mb20 relative">
                        <p class="fontEn pageRecruitFlowNum mainColor">01</p>
                        <div class="topWorksCate bgWhite absolute text_m bold">
                            <h5 class="bold h4 mainColor">ご応募</h5>
                        </div>
                    </div>
                    <p>履歴書と（あれば）職務経歴書をご送付ください。弊社から連絡させていただきます。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="topWorksBox mb50">
                    <div class="bgSubColor mb20 relative">
                        <p class="fontEn pageRecruitFlowNum mainColor">02</p>
                        <div class="topWorksCate bgWhite absolute text_m bold">
                            <h5 class="bold h4 mainColor">面接日の設定</h5>
                        </div>
                    </div>
                    <p>担当者と応募者のスケジュールをあわせ、 面接の日程をご相談させていただきます。 </p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="topWorksBox mb50">
                    <div class="bgSubColor mb20 relative">
                        <p class="fontEn pageRecruitFlowNum mainColor">03</p>
                        <div class="topWorksCate bgWhite absolute text_m bold">
                            <h5 class="bold h4 mainColor">合否の連絡</h5>
                        </div>
                    </div>
                    <p>面接後、7日～10日以内に合否の連絡をさせていただきます。</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin">
	<div class="container">
        <div class="mb50 text-center">
            <p class="fontEn h2 mb0 mainColor">Description</p>
            <h3 class="h3 bold titleBd inlineBlock">募集要項</h3>
        </div>
        <!--
		<div class="detail1 mb80">
			<div class="width720">
				<h4 class="h4 mb10 titleBdThin text-center inlineBock mb30">製造スタッフ(未経験者歓迎)</h4>
				<div class="mb30">
				
<p>当社の仕事は「ライン工」と呼ばれる「流れ作業」ではなく依頼された商品の図面を確認し、材料の調達→加工→検査→完成までを一貫して行います。</p>
<p>製造工程の中には<span class="bold">「NC旋盤」や「マシニング加工」</span>といった専門知識を要するものもありますが、まずは、先輩スタッフのもと、各工程の下準備などを行っていただきます。</p>
<p>スタッフ一人ひとりに対し、<span class="bold">各工程ごとで仕事の習得度を一覧にした「スキル管理表」</span>がありますので、少しずつお任せできる仕事を増やしていってください。</p>
<p>年月が経てば、技術者としての成長を実感できるとともに、日本経済の象徴である<span class="bold">「ものづくり」の真髄が学べる職場</span>です。</p>
				
				</div>
			</div>
			<div class="pageAboutCompanyUl width720 mb50" data-aos="fade-up">
				<ul>
					<li>勤務地</li>
					<li>総社市 東阿曽</li>
				</ul>
				<ul>
					<li>給与</li>
					<li>月給 15.6万 〜 30万円</li>
				</ul>
				<ul>
					<li>雇用形態</li>
					<li>正社員</li>
				</ul>
				<ul>
					<li>求める人材</li>
					<li>・高卒以上<br>・未経験者積極採用<br>・プログラミング経験者優遇</li>
				</ul>
				<ul>
					<li>勤務時間</li>
					<li>
						<p>【勤務時間】8:00~17:00　※変形労働時間制<br>【休　日】土曜、日曜</p>
						<p>※繁忙期には一部土曜出勤有　会社カレンダーがあります<br>GW、夏季休暇、年末年始休暇</p>
					</li>
				</ul>
				<ul>
					<li>待遇・福利厚生</li>
					<li>
						<p>・社会保険完備<br>・財形退職金制度　※勤続3年以上<br>・前年賞与実績　年2回<br>・給与は経験などを考慮します</p>
						<p>【手当】<br>・通勤手当　上限10,000円<br>・食事手当　1食130円<br>・家族手当　1人2,000円<br>・皆勤手当　5,000円<br>・役職手当　15,000円~<br>・管理手当　20,000円~<br>※諸条件有</p>
						
					</li>
				</ul>
				<ul>
					<li>その他</li>
					<li>・NC、マシニングなどの「技術」を身に着けることができる職場です。<br>・今後の会社の成長を一緒に支えてくれる若い力を求めています。<br>・職場見学、歓迎します。<br>・不明な点はお気軽にお問合せください</li>
				</ul>
			</div>
		</div>
        -->
        <div class="text-center mb30">
            <a href="<?php echo home_url();?>/contact" class="white h4 button bold tra text-center">松本工務店の求人に応募する</a>
        </div>
	</div>

</section>





<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>
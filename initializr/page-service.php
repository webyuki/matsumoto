<?php get_header(); ?>

<main>

<section class="pageHeader relative">
    <div class="pageHeaderImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_service_fv.jpg');"></div>
    <div class="pageHeaderText absolute bgTraColorDeep">
        <h2 class="h2 bold mb10">サービス内容</h2>
        <p class="fontEn h1 mainColor">Service</p>
    </div>
</section>

<section class="margin">
    <div class="container">
        <ul class="pageServiceButtonUl flex justCenter">
            <li>
                <div class="text-center">
                    <a href="/#reform" class="white button h4 mb10 bold tra text-center">リフォーム</a>
                </div>
            </li>
            <li>
                <div class="text-center">
                    <a href="/#build" class="white button h4 mb10 bold tra text-center">新築施工</a>
                </div>
            </li>
            <li>
                <div class="text-center">
                    <a href="/#shop" class="white button h4 mb10 bold tra text-center">店舗・事務所・公共施設</a>
                </div>
            </li>
        </ul>
    </div>
</section>



<section class="relative" id="reform">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_service_bg_01.jpg">
        <div class="pageServiceTitleBoxWrpa">
            <div class="bgTraColorDeep pageServiceTitleBox">
                <div class="boxInnerBd">
                    <div class="text-center">
                        <p class="fontEn h2 mainColor">Reform</p>
                        <h3 class="h3 bold mb30">リフォーム</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="topRecruit margin relative" >
    <div class="container">
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6">
                <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">予算・住み心地・使いやすさ<br>を叶えるリフォーム</h4>
                <div class="mb30">

<p>松本工務店では、キッチン・お風呂・洗面所などの水まわりを中心に、リビングや外構、その他お住いのあらゆる場所を、安全で使いやすくするためのリフォーム工事を承っています。</p>
<p>部分的なリフォームにとどまらず、新築・増築やリノベーションなど大がかりな施工も手がけています。毎日の暮らしを、より快適にするためのお手伝いはお任せください。</p>


                </div>
            </div>
            <div class="col-sm-6">
                <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_service_reform_01.jpg" alt="リフォーム">
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="text-center">
            <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">松本工務店の<br>リフォームへのこだわり</h4>
        </div>
        <div class="row mb30" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">01</p>
                    <h5 class="mainColor text-center h4 bold mb30">予算や希望に沿った<br>プランをご提案</h5>
                    <p class="text_m gray">リフォーム最大のお悩みは、高額になりがちな費用。弊社ではお客さまのご予算とご希望に合うプランを、個別でご提案しています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">02</p>
                    <h5 class="mainColor text-center h4 bold mb30">60年かけて培った<br>幅広い知識でサポート</h5>
                    <p class="text_m gray">デザインや仕様など、設備には様々な特徴があります。弊社では長い実績の中で蓄積した知識をいかし、お客さまの最適な選択をサポートいたします。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">03</p>
                    <h5 class="mainColor text-center h4 bold mb30">1日も早く「いつも<br class="pc">通りの生活」を回復</h5>
                    <p class="text_m gray">工事中は、お客さまに多少のご不便をおかけします。弊社ではお客さまが1日も早くいつも通りの生活を取り戻せるよう、効率のよい施工を行っています。</p>
                </div>
            </div>
        </div>
        <div class="text-center mb30">
            <a href="<?php echo home_url();?>/works" class="white button h4 bold tra text-center">リフォームの施工事例</a>
        </div>
    </div>
</section>


<section class="relative" id="build">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_service_bg_02.jpg">
        <div class="pageServiceTitleBoxWrpa">
            <div class="bgTraColorDeep pageServiceTitleBox">
                <div class="boxInnerBd">
                    <div class="text-center">
                        <p class="fontEn h2 mainColor">Build</p>
                        <h3 class="h3 bold mb30">新築施工</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="topRecruit margin relative">
    <div class="container">
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6">
                <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">冬あたたかく、夏さわやかに<br>快適性を重視した家づくり
</h4>
                <div class="mb30">

<p>松本工務店では、住みやすさを重視した家づくりをしています。断熱性を考慮した設計により、夏はさわやかに、冬はあたたかい居住性を実現。</p>
<p>ご家族のライフスタイルに合わせて、柔軟に使える収納にも対応しています。</p>
<p>さらに万全なアフターメンテナンスにより、地域でのご家族の暮らしを、長くしっかりとサポートしてまいります。</p>

                </div>
            </div>
            <div class="col-sm-6">
                <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_service_build_01.jpg" alt="新築施工">
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="text-center">
            <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">松本工務店の<br>新築施工へのこだわり</h4>
        </div>
        <div class="row mb30" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">01</p>
                    <h5 class="mainColor text-center h4 bold mb30">高い断熱性で<br>快適な居住空間を実現</h5>
                    <p class="text_m gray">外壁の内側と屋根の下、天井上には7.5~10cmの断熱材を使用し、夏の暑さと冬の寒さを遮断しています。外気の影響を受けにくく、電気代も抑えます。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">02</p>
                    <h5 class="mainColor text-center h4 bold mb30">豊富な収納で広い<br>居住スペースを確保</h5>
                    <p class="text_m gray">居住スペースはそのままに、季節ものやお子さまの服やおもちゃなど、場所をふさいでしまいやすい物もしっかり収納できるプランをご提案しています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">03</p>
                    <h5 class="mainColor text-center h4 bold mb30">ご予算と希望に合わせた<br>プランをご提案</h5>
                    <p class="text_m gray">「予算は絶対オーバーしたくない」、「コストパフォーマンスを重視したい」など、お客さまのご要望に合わせ、柔軟にプランを作成しています。</p>
                </div>
            </div>
        </div>
        <div class="text-center mb30">
            <a href="<?php echo home_url();?>/works" class="white button h4 bold tra text-center">新築施工の施工事例</a>
        </div>
    </div>
</section>


<section class="relative" id="shop">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/top_service_bg_03.jpg">
        <div class="pageServiceTitleBoxWrpa">
            <div class="bgTraColorDeep pageServiceTitleBox">
                <div class="boxInnerBd">
                    <div class="text-center">
                        <p class="fontEn h2 mainColor">Business</p>
                        <h3 class="h3 bold mb30">店舗・事務所・公共施設</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="topRecruit margin relative">
    <div class="container">
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6">
                <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">店舗・事務所・公共施設など、<br>地域密着型の豊富な施工実績</h4>
                <div class="mb30">

<p>松本工務店ではこれまで、学校やプール、病院（診療所・クリニック）、保育園などの公共施設や、店舗や事務所などの建築を手がけてきました。</p>
<p>長きに渡り、法を遵守した手続きと厳しい審査に対応し、安全第一で施工を進めるノウハウを蓄積。お子さまからお年寄りまで、地域の皆さまが日々安心して使用できる施設を建設しています。</p>


                </div>
            </div>
            <div class="col-sm-6">
                <img class="mb30" src="<?php echo get_template_directory_uri();?>/img/page_service_business_01.jpg" alt="店舗・事務所・公共施設">
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="text-center">
            <h4 class="mainColor bold h3 mb30 titleBdThin inlineBlock">松本工務店の<br>設計・建設工事へのこだわり</h4>
        </div>
        <div class="row mb30" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">01</p>
                    <h5 class="mainColor text-center h4 bold mb30">公共工事の<br>厳しいルールに対応</h5>
                    <p class="text_m gray">公共工事では、工事に関わる様々な事柄が厳密に規定されています。弊社では現場での安全管理や書類上の手続きなど、法を順守した工事を進めています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">02</p>
                    <h5 class="mainColor text-center h4 bold mb30">安全を最優先した<br>工程と現場管理</h5>
                    <p class="text_m gray">建築に関わる職人だけでなく、施設を利用する全ての人に気持ち良く過ごしていただくため、現場での安全は最優先しています。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageServiceCommitBox bgSubColor mb30 matchHeight">
                    <p class="fontEn mainColor text-center pageServiceCommitNum">03</p>
                    <h5 class="mainColor text-center h4 bold mb30">公共施設での工事に慣れた<br>技術力のある職人を選出
</h5>
                    <p class="text_m gray">弊社では高い技術力に加え、現場での柔軟な対応やコミュニケーション能力の高い職人を採用し、ハイクオリティな工事を行っています。</p>
                </div>
            </div>
        </div>
        <div class="text-center mb30">
            <a href="<?php echo home_url();?>/works" class="white button h4 bold tra text-center">店舗・事務所・公共施設の施工事例</a>
        </div>
    </div>
</section>





<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>
<?php get_header(); ?>

<main>

<section class="bgMainColor pageHeader">
	<div class="container">
		<div class="text-center white">
			<div class="inlineBlock">
				<p class="fontEn h00 titleBdWhite mb30">STAFF</p>
				<h3 class="h3">社員紹介</h3>
			</div>
		</div>
	</div>
</section>

<section class="topRecruit margin">
	<div class="flex flexPc">
		<div class="flexS60 flexInner bgMainColor flexSplit white"  data-aos="fade-right">
			<div class="mb50">
				<h4 class="h3 titleBdWhite bold mb30 inlineBlock">岡田精工の良心として</h4>
				<p class="h3 mb10">坂本 達也<span class="h4">　（品質管理課　課長）</span></p>
				<p class="text_m bold">1995年 中途採用</p>
			</div>
			<img class="mb10 sp" src="<?php echo get_template_directory_uri();?>/img/page_staff_01.jpg" alt="">
			<div class="text_m">

<p>機械の組立に携わった前職から、岡田精工の製造課に入社し、気がつくと24年もたっていました。
製造の現場と品質管理双方の経験をいかし、岡田精工の「目」として、お客さまに信頼していただける品質を守ります。
</p>
			</div>
		</div>
		<div class="flexS60 bgImg padding flexSplit pc" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_staff_01.jpg')"  data-aos="fade-left"></div>
	</div>
</section>

<section class="margin">
	<div class="container">
	
		<div class="row mb30">
			<div class="col-sm-6"  data-aos="fade-right">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_02.jpg" alt="">
			</div>
			<div class="col-sm-6"  data-aos="fade-left">
				<p class="gray bold text_m">Q1.やりがいを感じるのはどんなとき？</p>
				<h4 class="h3 bold mainColor mb30">関わったすべての仕事が結果につながる</h4>
				<div>
				
<p>私が所属する品質管理課は、一般的に製品が完成した後を担当する部署です。一方弊社の品質管理課は、お客さまからのご注文をお受けした直後、作業工程や加工の方法を決める段階から製造に関わります。</p>

<p>具体的には、お客さまからお預かりした図面への実際の作業に必要な情報の追加、加工された製品のチェック、出荷までの管理、出荷後のお客さま対応などです。</p>

<p>	工程や加工の方法を改善したことで不良品が減った、生産効率が上がったなど、小さな積み重ねが結果につながったときにやりがいを感じます。</p>			
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6"  data-aos="fade-left">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_03.jpg" alt="">
			</div>
			<div class="col-sm-6 col-sm-pull-6"  data-aos="fade-left">
				<p class="gray bold text_m">Q2.応募者へのメッセージ</p>
				<h4 class="h3 bold mainColor mb30">視野を広く持ち、新たな未来をひらいてほしい</h4>
				<div>
				
<p>この会社には、前向きな気持ちで「やりたい」と言ったことをサポートしてくれる環境があります。もちろん、それに対する必要性や結果は求められます。けれど失敗も含め、社員の挑戦は応援してくれる会社です。</p>

<p>これから私たちの仲間となる方には、1日も早く業務に慣れ、視野を広げて新たな可能性を見つけてほしいと思っています。この仕事には、自ら動くことで生まれるチャンスがまだまだあります。ぜひ一緒にものづくりの未来をひらいていきましょう。</p>			
				</div>
			</div>
		</div>
	
	</div>
</section>

<section class="topRecruit margin">
	<div class="flex flexPc">
		<div class="flexS60 flexInner bgMainColor flexSplit white"  data-aos="fade-right">
			<div class="mb50">
				<h4 class="h3 titleBdWhite bold mb30 inlineBlock">ものづくりの喜びを次世代に</h4>
				<p class="h3 mb10">山科　次郎<span class="h4">　（製造課　課長）</span></p>
				<p class="text_m bold">2005年 中途採用</p>
			</div>
			<img class="mb10 sp" src="<?php echo get_template_directory_uri();?>/img/page_staff_04.jpg" alt="">
			<div class="text_m">

<p>”母なる機械”の異名を持つＮＣ旋盤に惹かれ、長く機械製造に携わってきました。
ものづくりの喜びと、多くの人に必要とされる技術をこれからの世代に伝えていきたいと思っています。
</p>
			</div>
		</div>
		<div class="flexS60 bgImg padding flexSplit pc" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_staff_04.jpg')"  data-aos="fade-left"></div>
	</div>
</section>

<section class="margin">
	<div class="container">
	
		<div class="row mb30">
			<div class="col-sm-6"  data-aos="fade-right">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_05.jpg" alt="">
			</div>
			<div class="col-sm-6"  data-aos="fade-left">
				<p class="gray bold text_m">Q1.やりがいを感じるのはどんなとき？</p>
				<h4 class="h3 bold mainColor mb30">図面のイメージが形になる喜び</h4>
				<div>
				
<p>私は何度か転職をしながらも、NC旋盤での加工業務に携わってきました。鉄を削るという仕事が、単純でありながらどこまでも技術を高めていけることや、NC旋盤があれば、どんな機械でも作れること。その可能性の大きさに惹かれ、現場での仕事に打ち込んできました。</p>

<p>現場では、お客さまからお預かりした図面に沿って、自分で工程と削り方を考えてプログラムを作ります。頭のなかでイメージしていたものが形になったときの、喜びや達成感はかけがえのないものです。</p>			
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6"  data-aos="fade-left">
				<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_staff_06.jpg" alt="">
			</div>
			<div class="col-sm-6 col-sm-pull-6"  data-aos="fade-left">
				<p class="gray bold text_m">Q2.応募者へのメッセージ</p>
				<h4 class="h3 bold mainColor mb30">努力が正しく評価され、自分の力を試せる環境に</h4>
				<div>
				
<p>弊社では現在、受注から出荷までを見える化し、日々の生産実績や生産効率を数字で見られるシステムを開発しています。大きな目的は会社全体として生産効率を上げていくことですが、個人の業績も数字で見えるようになります。</p>

<p>これまではおおよそでしか把握できていなかったことが、具体的な数値で表され、個人の努力が正しく評価されるようになります。既存の社員にはもちろん、これから仲間になる方にも、会社を信頼し、未来を担う仲間として一緒にがんばってほしいと思います。</p>			
				</div>
			</div>
		</div>
	
	</div>
</section>



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>
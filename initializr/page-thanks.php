<?php get_header(); ?>

<main>
<section class="pageHeader relative">
    <div class="pageHeaderImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_works_fv.jpg');"></div>
    <div class="pageHeaderText absolute bgTraColorDeep">
        <h2 class="h2 bold mb10">送信完了</h2>
        <p class="fontEn h1 mainColor">Thanks</p>
        <p class="fontEn h1 mainColor">Thanks</p>
    </div>
</section>



<section class="margin">
	<div class="container">
		<div class="">
			<div class="contInCont" data-aos="fade-up">
				<div class="mb30 text-center width780">
					<p>お問い合わせの送信が完了しました。</p>
					<p>担当者から折り返させてご連絡させて頂きますので、今しばらくお待ち下さい。</p>
				</div>
				<a class="telLink fontEn h0 text-center bold mainColor block mb0" href="tel:0869250084"><i class="fa fa-phone" aria-hidden="true"></i>0869-25-0084</a>
            <p class="gray text-center">AM9：00～PM5：00 （日曜、祝日除く）</p>
            <p class="mb30 text-center fontEn h2">FAX : 0869-25-0196</p>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="109"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>
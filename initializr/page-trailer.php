<?php get_header(); ?>

<main>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_trailer_fv.jpg">
        <div class="bgWhiteTrans paddingW">
            <div class="container" data-aos="fade-up">
                <div class="text-center">
                    <p class="fontEn h3 mb0 mainColor">TRAILER HOUSE</p>
                    <h3 class="h2 bold">トレーラーハウス</h3>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">WHAT IS TRAILER HOUSE?</p>
            <h3 class="h3 bold">トレーラーハウスとは？</h3>
        </div>
        <div class="row mb10" data-aos="fade-up">
            <div class="col-sm-6 col-sm-push-6">
                <div class="">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_trailer_01.jpg" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
                <h4 class="topServiceTitle relative bold h3 mb30"><span class="bold">別荘・離れに最適！<br class="pc">リフォームがお手軽に</span></h4>
                <div class="mb30">

<p>トレーラーハウスは、木造2×4（ツーバイフォー）工法の高気密・高断熱です。</p>
<p>幅2.4ｍ×長さ5mの最小サイズから、幅3.4ｍ×長さ12ｍまでの最大サイズまで、利用する用途（住宅・別荘・店舗・事務所等）または、土地の広さに合わせて設置する事が出来ます。</p>

<p>被災地でも有効活用されており、様々な用途で利用・活用ができます。 建築物でも、自動車でも製造基準があります。それは利用者の安心と安全を担保するためです。</p>
<p>トレーラーハウスにも同様、基準があります。弊社の製品は、日本RV輸入協会の構造基準を満たしております。</p>


                </div>
            </div>
        </div>
    </div>
</section>





<section class="margin">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">MERIT</p>
            <h3 class="h3 bold">トレーラーハウスのメリット</h3>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox trailer mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_trailer_merit_ico_01.png" alt="" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">コストの削減</h4>
                    <p>トレーラーハウスは、市街地化調整区域など一般的に建築物が建てられない場所にも設置さえ出来れば住居、店舗などの様々な形で利用できます。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox trailer mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_trailer_merit_ico_02.png" alt="" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">移動・移転が容易に可能</h4>
                    <p>トレーラーハウスは、市街地化調整区域など一般的に建築物が建てられない場所にも設置さえ出来れば住居、店舗などの様々な形で利用できます。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox trailer mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_trailer_merit_ico_03.png" alt="" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">様々な場所に設置可能</h4>
                    <p>トレーラーハウスは、市街地化調整区域など一般的に建築物が建てられない場所にも設置さえ出来れば住居、店舗などの様々な形で利用できます。</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox trailer mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_trailer_merit_ico_01.png" alt="" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">コストの削減</h4>
                    <p>トレーラーハウスは、市街地化調整区域など一般的に建築物が建てられない場所にも設置さえ出来れば住居、店舗などの様々な形で利用できます。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox trailer mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_trailer_merit_ico_02.png" alt="" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">移動・移転が容易に可能</h4>
                    <p>トレーラーハウスは、市街地化調整区域など一般的に建築物が建てられない場所にも設置さえ出来れば住居、店舗などの様々な形で利用できます。</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox trailer mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_trailer_merit_ico_03.png" alt="" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">様々な場所に設置可能</h4>
                    <p>トレーラーハウスは、市街地化調整区域など一般的に建築物が建てられない場所にも設置さえ出来れば住居、店舗などの様々な形で利用できます。</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="padding bgMainColorLight">
    <div class="container">
        <div class="text-center">
            <p class="fontEn h4 mb0 mainColor">POINT</p>
            <h3 class="h3 bold mb50">ビジネスにもプライベートにも</h3>
        </div>
        <div class="row" data-aos="fade-up">
            <div class="col-sm-6">
                <div class="topServiceBox mb50">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_trailer_02.jpg" alt="">
                    <h4 class="topServiceTitle relative bold h4 mb30 matchHeight"><span class="bold">住居や生活空間として、<br class="pc">または宿泊・滞在用などの活用</span></h4>
                    <ul class="pageTrailerUl gray">
                        <li>自宅に子供部屋を作ってあげたい</li>
                        <li>自宅に子供部屋を作ってあげたい</li>
                        <li>自宅に子供部屋を作ってあげたい</li>
                        <li>自宅に子供部屋を作ってあげたい</li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="topServiceBox mb50">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_trailer_03.jpg" alt="">
                    <h4 class="topServiceTitle relative bold h4 mb30 matchHeight"><span class="bold">事業用、店舗としての活用</span></h4>
                    <ul class="pageTrailerUl gray">
                        <li>自宅に子供部屋を作ってあげたい</li>
                        <li>自宅に子供部屋を作ってあげたい</li>
                        <li>自宅に子供部屋を作ってあげたい</li>
                        <li>自宅に子供部屋を作ってあげたい</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="text-center">
            <p class="fontEn h4 mb0 mainColor">TYPE</p>
            <h3 class="h3 bold mb50">各タイプの紹介</h3>
        </div>
        <div class="row mb30">
            <div class="col-sm-6">
                <div class="pageTrailerTytpeSlick mb30">
                    <ul class="slickThumbJs mb20">
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                    </ul>
                    <ul class="slickThumbJsNav">
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                    </ul>
                
                </div>
            
            </div>
            <div class="col-sm-6">
                <h4 class="pageTrailerTypeTitle"><span class="pageTrailerTypeTitleCate bgMainColor white text_m bold">居住用</span><span class="h3 bold">ロイヤル</span></h4>
                <h5 class="mainColor h3 bold mb30"><span class="fontEn h1">7,020,000</span>円〜</h5>
                <div class="mb30">
<p>ロイヤルの名前の通り豪華、高級感の意味。トレーラーハウスの最高峰として最新技術を結集して、日々進化している 世界に誇れるモデルです。</p>
<p>ロフト部分の吹き抜けや、ふんだんに取り入れた窓から、光がいっぱいに降り注ぐ室内は、シーリングファンや ブラケットライト等の沢山の照明器具が標準で装備され豪華な室内を演出しています。</p>
<p>高気密・高断熱を実現、バリアフリー・床暖房等の装備も可能です。</p>
<p>活用方法：コテージ・住居・一般的な離れ・宿泊用・別荘・介護用・リゾート・災害時の仮設等 ロイヤルをベースに各種フロアプランが製造可能です。</p>

                
                </div>
            </div>
        </div>
        <div class="row mb30">
            <div class="col-sm-6">
                <div class="pageTrailerTytpeSlick mb30">
                    <ul class="slickThumbJs mb20">
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                    </ul>
                    <ul class="slickThumbJsNav">
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                    </ul>
                
                </div>
            
            </div>
            <div class="col-sm-6">
                <h4 class="pageTrailerTypeTitle"><span class="pageTrailerTypeTitleCate bgMainColor white text_m bold">居住用</span><span class="h3 bold">ロイヤル</span></h4>
                <h5 class="mainColor h3 bold mb30"><span class="fontEn h1">7,020,000</span>円〜</h5>
                <div class="mb30">
<p>ロイヤルの名前の通り豪華、高級感の意味。トレーラーハウスの最高峰として最新技術を結集して、日々進化している 世界に誇れるモデルです。</p>
<p>ロフト部分の吹き抜けや、ふんだんに取り入れた窓から、光がいっぱいに降り注ぐ室内は、シーリングファンや ブラケットライト等の沢山の照明器具が標準で装備され豪華な室内を演出しています。</p>
<p>高気密・高断熱を実現、バリアフリー・床暖房等の装備も可能です。</p>
<p>活用方法：コテージ・住居・一般的な離れ・宿泊用・別荘・介護用・リゾート・災害時の仮設等 ロイヤルをベースに各種フロアプランが製造可能です。</p>

                
                </div>
            </div>
        </div>
        <div class="row mb30">
            <div class="col-sm-6">
                <div class="pageTrailerTytpeSlick mb30">
                    <ul class="slickThumbJs mb20">
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                    </ul>
                    <ul class="slickThumbJsNav">
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                    </ul>
                
                </div>
            
            </div>
            <div class="col-sm-6">
                <h4 class="pageTrailerTypeTitle"><span class="pageTrailerTypeTitleCate bgMainColor white text_m bold">居住用</span><span class="h3 bold">ロイヤル</span></h4>
                <h5 class="mainColor h3 bold mb30"><span class="fontEn h1">7,020,000</span>円〜</h5>
                <div class="mb30">
<p>ロイヤルの名前の通り豪華、高級感の意味。トレーラーハウスの最高峰として最新技術を結集して、日々進化している 世界に誇れるモデルです。</p>
<p>ロフト部分の吹き抜けや、ふんだんに取り入れた窓から、光がいっぱいに降り注ぐ室内は、シーリングファンや ブラケットライト等の沢山の照明器具が標準で装備され豪華な室内を演出しています。</p>
<p>高気密・高断熱を実現、バリアフリー・床暖房等の装備も可能です。</p>
<p>活用方法：コテージ・住居・一般的な離れ・宿泊用・別荘・介護用・リゾート・災害時の仮設等 ロイヤルをベースに各種フロアプランが製造可能です。</p>

                
                </div>
            </div>
        </div>
        <div class="row mb30">
            <div class="col-sm-6">
                <div class="pageTrailerTytpeSlick mb30">
                    <ul class="slickThumbJs mb20">
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                    </ul>
                    <ul class="slickThumbJsNav">
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                        <li>
                            <img class="" src="<?php echo get_template_directory_uri();?>/img/top_service_trailer.jpg" alt="">
                        </li>
                    </ul>
                
                </div>
            
            </div>
            <div class="col-sm-6">
                <h4 class="pageTrailerTypeTitle"><span class="pageTrailerTypeTitleCate bgMainColor white text_m bold">居住用</span><span class="h3 bold">ロイヤル</span></h4>
                <h5 class="mainColor h3 bold mb30"><span class="fontEn h1">7,020,000</span>円〜</h5>
                <div class="mb30">
<p>ロイヤルの名前の通り豪華、高級感の意味。トレーラーハウスの最高峰として最新技術を結集して、日々進化している 世界に誇れるモデルです。</p>
<p>ロフト部分の吹き抜けや、ふんだんに取り入れた窓から、光がいっぱいに降り注ぐ室内は、シーリングファンや ブラケットライト等の沢山の照明器具が標準で装備され豪華な室内を演出しています。</p>
<p>高気密・高断熱を実現、バリアフリー・床暖房等の装備も可能です。</p>
<p>活用方法：コテージ・住居・一般的な離れ・宿泊用・別荘・介護用・リゾート・災害時の仮設等 ロイヤルをベースに各種フロアプランが製造可能です。</p>

                
                </div>
            </div>
        </div>
    </div>
</section>

<script>


 $('.slickThumbJs').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slickThumbJsNav'
});
$('.slickThumbJsNav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slickThumbJs',
  dots: false,
  centerMode: true,
  focusOnSelect: true
});
	
</script>

<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_reform_voice_bg.jpg">
        <div class="bgWhiteTrans padding">
            <div class="container" data-aos="fade-up">
                <div class="text-center mb50">
                    <p class="fontEn h4 mb0 mainColor">VOICE</p>
                    <h3 class="h3 bold">お客様の声</h3>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/oage_reform_voice_01.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">キッチンから家族の様子を感じる事が出来るのが一番の魅力です。</h5>
                                <p class="grayLight bold">岡山市 N様</p>
                            </div>
                            <p class="text_m gray">さえぎるものがないのでキッチンから家族の様子を感じることができるのが一番の魅力です。家が完成し、これから家族や友人と集まって料理を楽しめたらいいなと思っていました。これからもいろいろな楽しみが生まれていきそうです。</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/oage_reform_voice_01.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">キッチンから家族の様子を感じる事が出来るのが一番の魅力です。</h5>
                                <p class="grayLight bold">岡山市 N様</p>
                            </div>
                            <p class="text_m gray">さえぎるものがないのでキッチンから家族の様子を感じることができるのが一番の魅力です。家が完成し、これから家族や友人と集まって料理を楽しめたらいいなと思っていました。これからもいろいろな楽しみが生まれていきそうです。</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pageReformVoiceBox bgWhite bdBox mb50">
                            <div class="pageReformVoiceImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/oage_reform_voice_01.jpg');"></div>
                            <div class="text-center mb30">
                                <h5 class="mainColorLight h4 bold mb10">キッチンから家族の様子を感じる事が出来るのが一番の魅力です。</h5>
                                <p class="grayLight bold">岡山市 N様</p>
                            </div>
                            <p class="text_m gray">さえぎるものがないのでキッチンから家族の様子を感じることができるのが一番の魅力です。家が完成し、これから家族や友人と集まって料理を楽しめたらいいなと思っていました。これからもいろいろな楽しみが生まれていきそうです。</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>
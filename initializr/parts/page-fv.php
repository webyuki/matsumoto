


<?php if(is_page_template('page-menu.php')): ?>

<?php $slug = $post -> post_name; ?>

	  <?php $args = array(
		'numberposts' => 1, //表示する記事の数
		'post_type' => 'do_fv', //投稿タイプ名
		// 条件を追加する場合はここに追記
	  );
	  $customPosts = get_posts($args);
	  if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post );
		$image01 = get_post_meta($post->ID, $slug, true);
		$thumb = wp_get_attachment_image_src($image01, 'full');
	  ?>
	  <?php if ( $thumb ):?>
		<img src="<?php echo $thumb[0];?>" class="img-responsive">
	<?php endif;?>

	  <?php endforeach; ?>
	  <?php else : //記事が無い場合 ?>
	  <p>Sorry, no posts matched your criteria.</p>
	  <?php endif;
	  wp_reset_postdata(); //クエリのリセット ?>

<?php elseif(is_singular('menu') ):?>

<?php
if ($terms = get_the_terms($post->ID, 'menu_tax')) {
    foreach ( $terms as $term ) {
        $slug = esc_html($term->slug);
        $name = esc_html($term->name);
    }
}
?>
	
	  <?php $args = array(
		'numberposts' => 1, //表示する記事の数
		'post_type' => 'do_fv', //投稿タイプ名
		// 条件を追加する場合はここに追記
	  );
	  $customPosts = get_posts($args);
	  if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post );
		$image01 = get_post_meta($post->ID, $slug, true);
		$thumb = wp_get_attachment_image_src($image01, 'full');
	  ?>
	  <?php if ( $thumb ):?>
		<img src="<?php echo $thumb[0];?>" class="img-responsive">
	<?php endif;?>

	  <?php endforeach; ?>
	  <?php else : //記事が無い場合 ?>
	  <p>Sorry, no posts matched your criteria.</p>
	  <?php endif;
	  wp_reset_postdata(); //クエリのリセット ?>



<?php endif;?>

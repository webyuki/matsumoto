<?php
$terms = get_the_terms($post->ID, 'menu_tax');
    foreach ( $terms as $term ) {
        $slug = esc_html($term->slug);
        $name = esc_html($term->name);
    }
?>	

	<aside>
    	<?php $args = array(
			'numberposts' => 5, //表示する記事の数
			'post_type' => 'example', //投稿タイプ名
			// 条件を追加する場合はここに追記
			'tax_query' => array(
				array(
					'taxonomy' => 'cat_example',
					'field' => 'slug',
					'terms' => $slug,
					/*'operator'  => 'NOT IN'*/
				)
			)				
		  );
		  $customPosts = get_posts($args);
		 ?>
         
         		<?php if($customPosts) : ?>
		<h2 class="h3 title_main  bold title_margin">最新施工事例</h2>
        	  <?php endif; ?>
		<ul>
		<?php
		  
		  if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post );
		  ?>
		  <li><a href="<?php the_permalink(); ?>">> <?php the_title(); ?></a></li>
		  <?php endforeach; ?>
		  <?php else : //記事が無い場合 ?>
		  <!--<p>Sorry, no posts matched your criteria.</p>-->
		  <?php endif;
		  wp_reset_postdata(); //クエリのリセット ?>
		</ul>	
	</aside>

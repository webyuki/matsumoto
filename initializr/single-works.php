<?php get_header(); ?>

<?php 
	while ( have_posts() ) : the_post();
?>

<?php 
	// テキスト・ウィジェット関係
    /*
	$demand = get_field("demand");
	$advice = get_field("advice");
	$address = get_field("address");
	$period = get_field("period");
	$age = get_field("age");
	$price = get_field("price");
	$item = get_field("item");
	$exam_1_text = get_field("exam_1_text");
	$exam_2_text = get_field("exam_2_text");
	$voice_text = get_field("voice_text");
	$imgs = get_field("imgs");
    */
	// メイン画像
	if ( has_post_thumbnail() ) {
		$image_id = get_post_thumbnail_id();
		$image_url = wp_get_attachment_image_src ($image_id,'large',true);
	} else {
	}  
?>


<main>
<section class="pageHeader relative">
    <div class="pageHeaderImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_about_fv.jpg');"></div>
    <div class="pageHeaderText absolute bgTraColorDeep">
        <h2 class="h2 bold mb10">施工事例</h2>
        <p class="fontEn h1 mainColor">Works</p>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="text-center mb30">
            <h2 class="h3 bold mb10"><?php the_title(); ?></h2>
            <ul class="singleWorksCateUl grayLight inlineBlockUl">
                <li>
                    <?php 
                        if ($terms = get_the_terms($post->ID, 'works_area')) {
                            foreach ( $terms as $term ) {
                                echo '<span class="">' . esc_html($term->name) . '</span>';
                                $term_slug = $term->slug;
                            }
                        }
                    ?>
                </li>
                <li>
                    <?php 
                        if ($terms = get_the_terms($post->ID, 'works_cat')) {
                            foreach ( $terms as $term ) {
                                echo '<span class="">' . esc_html($term->name) . '</span>';
                                $term_slug = $term->slug;
                            }
                        }
                    ?>
                </li>
            </ul>
        </div>
        <div class="width980 mb50">
        
            <?php if (has_post_thumbnail()):?>
                <?php 
                    // アイキャッチ画像のIDを取得
                    $thumbnail_id = get_post_thumbnail_id();
                    // mediumサイズの画像内容を取得（引数にmediumをセット）
                    $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
                    $eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
                ?>
                    <img class="" src="<?php echo $eye_img_s[0];?>" alt="">
                    <div class="detail_main_img bg-common" style="background-image: url('');"></div>
                <?php else: ?>
                    <img class="" src="<?php echo get_template_directory_uri(); ?>/img/sample01.png" alt="">
            <?php endif; ?>

        </div>
        <div class="row mb50">
            <div class="col-sm-7">
                <div class="entry">
                    <?php the_content();?>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="singleWorksData bgSubColor">
                    <p class="fontEn mainColor h4 text-center mb30">DATA</p>
                    <div class="singleWorksDataDl">
                        <dl class="dl-horizontal">
                            <dt>施工エリア</dt>
                            <dd>
                                <?php 
                                    if ($terms = get_the_terms($post->ID, 'works_area')) {
                                        foreach ( $terms as $term ) {
                                            echo '<span class="">' . esc_html($term->name) . '</span>';
                                            $term_slug = $term->slug;
                                        }
                                    }
                                ?>
                            </dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>施工カテゴリー</dt>
                            <dd>
                                <?php 
                                    if ($terms = get_the_terms($post->ID, 'works_cat')) {
                                        foreach ( $terms as $term ) {
                                            echo '<span class="">' . esc_html($term->name) . '</span>';
                                            $term_slug = $term->slug;
                                        }
                                    }
                                ?>
                            </dd>
                        </dl>
                        <dl class="dl-horizontal">
                            <dt>投稿日</dt>
                            <dd><?php echo get_the_date( 'Y年m月d日' ); ?></dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <p class="fontEn mainColor h4 singleWorksBaBd">BEFORE</p>
        <div class="row mb50">
        
            <?php $i = 1; ?>
            <?php while (get_field('before_img_0'.$i)) : ?>
                
                <div class="col-sm-3 col-xs-6">
                    <a href="<?php the_field('before_img_0'.$i); ?>" data-lightbox="lightbox" data-title="<?php the_field('before_text_0'.$i); ?>">
                        <div class="singleWorksBaBox mb30">
                            <div class="singleWorksBaImgWrap relative">
                                <div class="singleWorksBaImg bgImg tra" style="background-image:url('<?php the_field('before_img_0'.$i); ?>');"></div>
                            <div class="singleWorksBaTitle bgMainColor text_m white absolute tra"><?php the_field('before_text_0'.$i); ?></div>
                            </div>
                        </div>
                    </a>
                </div>
                

            <?php $i++; ?>
            <?php endwhile; ?>
        
        </div>
        <p class="fontEn mainColor h4 singleWorksBaBd">AFTER</p>
        <div class="row mb50">
        
            <?php $i = 1; ?>
            <?php while (get_field('after_img_0'.$i)) : ?>
                
                <div class="col-sm-3 col-xs-6">
                    <a href="<?php the_field('after_img_0'.$i); ?>" data-lightbox="lightbox" data-title="<?php the_field('after_text_0'.$i); ?>">
                        <div class="singleWorksBaBox mb30">
                            <div class="singleWorksBaImgWrap relative">
                                <div class="singleWorksBaImg bgImg tra" style="background-image:url('<?php the_field('after_img_0'.$i); ?>');"></div>
                            <div class="singleWorksBaTitle bgMainColor text_m white absolute tra"><?php the_field('after_text_0'.$i); ?></div>
                            </div>
                        </div>
                    </a>
                </div>
                

            <?php $i++; ?>
            <?php endwhile; ?>
        
        </div>
    </div>
</section>


<section class="margin">
    <div class="container">
        <div class="text-center">
            <p class="fontEn h4 mb0 mainColor">Others</p>
            <h3 class="h3 bold mb50">他の事例を見る</h3>
        </div>
        <div class="row mb30">
        
        	<?php
						$args = array(
						'post_type' => 'works', //投稿タイプ名
						'posts_per_page' => 3, //出力する記事の数
						'tax_query' => array(
						array(
						'taxonomy' => 'works_cat', //タクソノミー名
						'field' => 'slug',
						'terms' => $term_slug //タームのスラッグ
						)
						)
						);
					?>
                    <?php
                    $myposts = get_posts( $args );
                	foreach ( $myposts as $post ) : setup_postdata( $post );
					?>

			<?php get_template_part('content-post-works-archive'); ?>

            <?php endforeach; ?>

        </div>
        <div class="text-center">
            <a href="<?php echo home_url();?>/works" class="white button bold tra text-center">事例一覧</a>
        </div>
    </div>
</section>



</main>

<?php 
	endwhile;
?>	


<?php get_footer(); ?>



<?php get_header(); ?>

<main>

<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>



<section class="relative" id="">
	<div class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri();?>/img/page_works_fv.jpg">
        <div class="bgWhiteTrans paddingW">
            <div class="container" data-aos="fade-up">
                <div class="text-center">
                    <p class="fontEn h3 mb0 mainColor">WORKS</p>
                    <h3 class="h2 bold">「<?php single_term_title(); ?>」の事例紹介</h3>
                </div>
            </div>
        </div>
    </div>
</section>





<section class="pageWorkLi margin">
	<div class="container">
		<div class="pageWorksLiLiNavi mb50">
			<ul class="inlineBlockUl white archiveWorksCateUl text-center text_m bold">
			
			
				<li><a href="<?php echo home_url();?>/works">全て</a></li>
				<?php $categories = get_categories(array('taxonomy' => 'works_cat')); if ( $categories ) : ?>
					<?php foreach ( $categories as $category ): ?>
						<li><a href="<?php echo home_url();?>/works_cat/<?php echo esc_html( $category->slug);?>"><?php echo wp_specialchars( $category->name ); ?></a></li>
					<?php endforeach; ?>
				<?php endif; ?>
								
			</ul>
		</div>
        <div class="row">
			<?php			
				while ( have_posts() ) : the_post();
					get_template_part('content-post-works-archive'); 
				endwhile;
			?>
        </div>
		
		
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>








</main>


<?php get_footer(); ?>